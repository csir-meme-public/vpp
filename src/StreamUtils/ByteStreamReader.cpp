/** @file

MODULE						: ByteStreamReader

TAG								: BSR

FILE NAME					: ByteStreamReader.cpp

DESCRIPTION				: A byte stream reader implementation of the ByteStreamBase
										base class. Add the functionality to do the reading.

REVISION HISTORY	:
									: 

COPYRIGHT					: 

RESTRICTIONS			: 
===========================================================================
*/
#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#else
#include <stdio.h>
#endif

#include <string.h>
#include <memory.h>

#include "ByteStreamReader.h"

ByteStreamReader::ByteStreamReader()
{
}//end constructor.

ByteStreamReader::~ByteStreamReader()
{
}//end destructor.

/** Bytes available for reading.
Determine the number of valid bytes that are
available to read.
@return			: The num of bytes.
*/
int ByteStreamReader::NumAvailToRead(void)
{
	int diff = _byteSize - _bytePos;
	if(diff < 1)
		return(0);

	return(diff);
}//end NumAvailToRead.

/** Read a single byte.
Read from the current byte position in the stream.
@return			: The byte.
*/
int ByteStreamReader::Read(void)
{
	if(_bytePos >= _byteSize)
		return(0);

	int x = (int)(_byteStream[_bytePos++]);

	return(x);
}//end Read.

/** Read bytes from the stream.
Read multiple bytes from the current stream position.
@param num	: No. of bytes to read.
@param dst	: Buffer to read into.
@return			: Num of bytes read.
*/
int ByteStreamReader::Read(int num, void* dst)
{
	int toRead	= num;
	int canRead = _byteSize - _bytePos;
	// None available or overrun the stream.
	if(canRead < 1)
		return(0);
	// Not enough bytes in the stream.
	if(num > canRead)
		toRead = canRead;

	memcpy(dst, (const void *)(&(_byteStream[_bytePos])), toRead);
	_bytePos += toRead;

	return(toRead);
}//end Read.

/** Peek bytes in the stream.
Read multiple bytes from the specified stream position 
without disturbing the	current stream position.
@param loc	: Byte pos in stream.
@param num	: No. of bytes to read.
@param dst	: Buffer to read into.
@return			: Num of bytes read.
*/
int ByteStreamReader::Peek(int loc, int num, void* dst)
{
	int toRead	= num;
	int canRead = _byteSize - loc;
	// None available or overrun the stream.
	if(canRead < 1)
		return(0);
	// Not enough bytes in the stream.
	if(num > canRead)
		toRead = canRead;

	memcpy(dst, (const void *)(&(_byteStream[loc])), toRead);

	return(toRead);
}//end Peek.



