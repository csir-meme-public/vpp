/** @file

MODULE						: StreamChunkHeader

TAG								: SCH

FILE NAME					: StreamChunkHeader.h

DESCRIPTION				: A utility class to handle reading and writing chunk
										headers from and to byte streams. Chunk headers have
										a 4 byte id and a size. The id labels are defined 
										here.

REVISION HISTORY	:
									: 

COPYRIGHT					: (c)VICS 2000-2006  all rights resevered - info@videocoding.com

RESTRICTIONS			: The information/data/code contained within this file is 
										the property of VICS limited and has been classified as 
										CONFIDENTIAL.
===========================================================================
*/
#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#else
#include <stdio.h>
#endif

#include <string.h>
#include <memory.h>

#include "StreamChunkHeader.h"

/*
--------------------------------------------------------------------------------
	Constants.
--------------------------------------------------------------------------------
*/
const unsigned int StreamChunkHeader::Vic = 
	(unsigned int)( (unsigned int)(unsigned char)('.') | ((unsigned int)(unsigned char)('V') << 8) |
									((unsigned int)(unsigned char)('I') << 16) | ((unsigned int)(unsigned char)('C') << 24) );

const unsigned int StreamChunkHeader::Properties = 
	(unsigned int)( (unsigned int)(unsigned char)('P') | ((unsigned int)(unsigned char)('R') << 8) |
									((unsigned int)(unsigned char)('O') << 16) | ((unsigned int)(unsigned char)('P') << 24) );

const unsigned int StreamChunkHeader::StreamProperties = 
	(unsigned int)( (unsigned int)(unsigned char)('S') | ((unsigned int)(unsigned char)('T') << 8) |
									((unsigned int)(unsigned char)('P') << 16) | ((unsigned int)(unsigned char)('R') << 24) );

const unsigned int StreamChunkHeader::ContentDescription = 
	(unsigned int)( (unsigned int)(unsigned char)('C') | ((unsigned int)(unsigned char)('O') << 8) |
									((unsigned int)(unsigned char)('N') << 16) | ((unsigned int)(unsigned char)('T') << 24) );

const unsigned int StreamChunkHeader::Data = 
	(unsigned int)( (unsigned int)(unsigned char)('D') | ((unsigned int)(unsigned char)('A') << 8) |
									((unsigned int)(unsigned char)('T') << 16) | ((unsigned int)(unsigned char)('A') << 24) );

/*
--------------------------------------------------------------------------------
	Class construction.
--------------------------------------------------------------------------------
*/
StreamChunkHeader::StreamChunkHeader()
{
	_chunkHeader.id		= 0;
	_chunkHeader.size = 0;
}//end constructor.

StreamChunkHeader::~StreamChunkHeader()
{
}//end destructor.

/*
--------------------------------------------------------------------------------
	Class interface.
--------------------------------------------------------------------------------
*/
/** Read the chunck header.
Make sure there are enough bytes available before attempting to
read in the header. Otherwise read none.
@param bsr	: Use this stream reader.
@return			: Bytes read.
*/
int StreamChunkHeader::Read(IStreamReader* bsr)
{
	int available = bsr->NumAvailToRead();
	if(available < (2 * sizeof(unsigned int)))
		return(0);

	int bytesRead = bsr->Read(sizeof(unsigned int), (void *)(&_chunkHeader.id));
	bytesRead += bsr->Read(sizeof(unsigned int), (void *)(&_chunkHeader.size));

	return(bytesRead);
}//end Read.

/** Write the chunck header.
Check that there is enough space to write the header otherwise
do not try.
@param bsw	: Use this stream writer.
@return			: Bytes written.
*/
int StreamChunkHeader::Write(IStreamWriter* bsw)
{
	int available = bsw->NumAvailToWrite();
	if(available < (2 * sizeof(unsigned int)))
		return(0);

	int bytesWritten = bsw->Write(sizeof(unsigned int), (void *)(&_chunkHeader.id));
	bytesWritten += bsw->Write(sizeof(unsigned int), (void *)(&_chunkHeader.size));

	return(bytesWritten);
}//end Write.

/*
--------------------------------------------------------------
	Common utility methods.
--------------------------------------------------------------
*/
/** Read unterminated string from stream.
Strings are stored as a 16 bit size followed by packed
ASCII characters and need to be converted into a string.
@param str	: Load into this string.
@param bsr	: From this stream.
@return			: Bytes read.
*/
int StreamChunkHeader::ReadString(char** str, IStreamReader* bsr)
{
	unsigned short size;
	int bytesRead = bsr->Read(2, (void *)(&size));
	// Delete old before loading new.
	if(*str != NULL)
		delete[] *str;
	if(size > 0)
	{
		*str = new char[size + 1];
		bytesRead += bsr->Read(size, (void *)(*str));
		(*str)[size] = 0;	// Null terminate.
	}//end if size...
	else
	{
		*str = new char[2];
		strcpy(*str, (const char *)(" "));
	}//end else...

	return(bytesRead);
}//end ReadString.

/** Write unterminated string to stream.
Strings are stored as a 16 bit size followed by packed
ASCII characters and need to be converted from a string.
@param str	: Write this string.
@param bsw	: Into this stream.
@return			: Bytes written.
*/
int StreamChunkHeader::WriteString(char* str, IStreamWriter* bsw)
{
	unsigned short size = (unsigned short)(strlen((const char *)str));
	if(size < 1)
		return(0);

	int bytesWritten = bsw->Write(2, (void *)(&size));
	bytesWritten += bsw->Write(size, (void *)(str));

	return(bytesWritten);
}//end WriteString.
