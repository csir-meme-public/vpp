/** @file

MODULE						: ByteStreamWriter

TAG								: BSW

FILE NAME					: ByteStreamWriter.cpp

DESCRIPTION				: A byte stream writer implementation of the ByteStreamBase
										base class. Add the functionality to do the writing.

REVISION HISTORY	:
									: 

COPYRIGHT					: 

RESTRICTIONS			: 
===========================================================================
*/
#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#else
#include <stdio.h>
#endif

#include <string.h>
#include <memory.h>

#include "ByteStreamWriter.h"

ByteStreamWriter::ByteStreamWriter()
{
}//end constructor.

ByteStreamWriter::~ByteStreamWriter()
{
}//end destructor.

/** Bytes available for writing.
Determine the number of valid bytes that are
available to write.
@return			: The num of bytes.
*/
int ByteStreamWriter::NumAvailToWrite(void)
{
	int diff = _byteSize - _bytePos;
	if(diff < 1)
		return(0);

	return(diff);
}//end NumAvailToWrite.

/** Write a single byte.
Write to the current byte position into the stream.
@param val	: Byte value to write.
@return			: Byte was written.
*/
int ByteStreamWriter::Write(int val)
{
	if(_bytePos >= _byteSize)
		return(0);

	_byteStream[_bytePos++] = (unsigned char)val;

	return(1);
}//end Write.

/** Write bytes to the stream.
Write multiple byte into the current stream position.
@param num	: No. of bytes to write.
@param src	: Byte values to write.
@return			: Num bytes written.
*/
int ByteStreamWriter::Write(int num, void* src)
{
	int toWrite		= num;
	int canWrite	= _byteSize - _bytePos;
	// End or overrun the stream.
	if(canWrite < 1)
		return(0);
	// Not enough bytes in the stream.
	if(num > canWrite)
		toWrite = canWrite;

	memcpy((void *)(&(_byteStream[_bytePos])), (const void *)src, toWrite);
	_bytePos += toWrite;

	return(toWrite);
}//end Write.

/** Poke bytes to the stream.
Write multiple bytes into the specified stream position 
without disturbing the current stream position.
@param loc	: Byte pos in stream.
@param num	: No. of bytes to write.
@param src	: Byte values to write.
@return			: Num bytes written.
*/
int ByteStreamWriter::Poke(int loc, int num, void* src)
{
	int toWrite		= num;
	int canWrite	= _byteSize - loc;
	// End or overrun the stream.
	if(canWrite < 1)
		return(0);
	// Not enough bytes in the stream.
	if(num > canWrite)
		toWrite = canWrite;

	memcpy((void *)(&(_byteStream[loc])), (const void *)src, toWrite);

	return(toWrite);
}//end Poke.


