/** @file

MODULE				: FastInverse4x4ITImpl1

TAG						: FII4TI1

FILE NAME			: FastInverse4x4ITImpl1.cpp

DESCRIPTION		: A class to implement a fast inverse 4x4 2-D integer
								transform defined by the H.264 standard on the input. 
								It implements the IInverseDct interface. The pre-scaling
								is part of the inverse quantisation process.

COPYRIGHT			:	(c)CSIR 2007-2009 all rights resevered

LICENSE				: Software License Agreement (BSD License)

RESTRICTIONS	: Redistribution and use in source and binary forms, with or without 
								modification, are permitted provided that the following conditions 
								are met:

								* Redistributions of source code must retain the above copyright notice, 
								this list of conditions and the following disclaimer.
								* Redistributions in binary form must reproduce the above copyright notice, 
								this list of conditions and the following disclaimer in the documentation 
								and/or other materials provided with the distribution.
								* Neither the name of the CSIR nor the names of its contributors may be used 
								to endorse or promote products derived from this software without specific 
								prior written permission.

								THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
								"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
								LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
								A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
								CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
								EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
								PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
								PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
								LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
								NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
								SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

===========================================================================
*/
#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#else
#include <stdio.h>
#endif

#include <string.h>
#include "FastInverse4x4ITImpl1.h"

/*
---------------------------------------------------------------------------
	Constants.
---------------------------------------------------------------------------
*/
const int FastInverse4x4ITImpl1::NormAdjust[6][3] =
{
	{10, 16, 13 },
	{11, 18, 14 },
	{13, 20, 16 },
	{14, 23, 18 },
	{16, 25, 20 },
	{18, 29, 23 }
};

const int FastInverse4x4ITImpl1::ColSelector[16] =
{
	0, 2, 0, 2 ,
	2, 1, 2, 1 ,
	0, 2, 0, 2 ,
	2, 1, 2, 1 
};

/*
--------------------------------------------------------------------------
	Construction.
--------------------------------------------------------------------------
*/
FastInverse4x4ITImpl1::FastInverse4x4ITImpl1(void)
{
	int i,j,qm;

	for(i = 0; i < 16; i++)
		_weightScale[i] = 16;	///< For baseline Flat_4x4_16 is default.

	for(qm = 0; qm < 6; qm++)
		for(i = 0; i < 4; i++)
			for(j = 0; j < 4; j++)
			{
				int pos = 4*i + j;
				_levelScale[qm][pos] = NormAdjust[qm][ColSelector[pos]] * _weightScale[pos];
			}//end for qm & i & j...

	_mode		= TransformAndQuant;
	_q			= 1;
	_qm			= _q % 6;
	_qe			= _q/6;
	_f			= 1 << (3-_qe);
	_leftScale	= _qe-4;
	_rightScale	= 4-_qe;

}//end constructor.

/*
---------------------------------------------------------------------------
	Interface Methods.
---------------------------------------------------------------------------
*/
/** In-place inverse Integer Transform.
The 2-D inverse IT is performed on the input coeffs and replaces them. A 1-D
inverse transform is performed on the rows first and then the cols. 
@param ptr	: Data to transform.
@return			:	none.
*/
void FastInverse4x4ITImpl1::InverseTransform(void* ptr)
{
	short* block = (short *)ptr;
	int j;

	if(_mode == QuantOnly)	///< 2 = Q only.
	{
	  /// Pre-scaling and quantisation are combined.
	  int i;
	  if (_q < 24)
	  {
		  for (i = 0; i < 16; i++)
		    block[i] = (short)((((int)block[i] * _levelScale[_qm][i]) + _f) >> _rightScale);
	  }//end if _q...
	  else
	  {
		  for (i = 0; i < 16; i++)
        block[i] = (short)(((int)block[i] * _levelScale[_qm][i]) << _leftScale);
	  }//end else...
	}//end if _mode == 2...
	else if(_mode == TransformAndQuant)	///< 0 = IT&Q.
	{
		/// 1-D inverse IT in horiz direction.
    if (_q < 24)
    {
      for (j = 0; j < 16; j += 4)
      {
        /// 1st stage with pre-scaling and quantisation.
        int x0 = (((int)block[j] * _levelScale[_qm][j]) + _f) >> _rightScale;
        int x1 = (((int)block[j + 1] * _levelScale[_qm][j + 1]) + _f) >> _rightScale;
        int x2 = (((int)block[j + 2] * _levelScale[_qm][j + 2]) + _f) >> _rightScale;
        int x3 = (((int)block[j + 3] * _levelScale[_qm][j + 3]) + _f) >> _rightScale;

        /// 2nd stage.
        block[j] = (short)((x0 + x2) + (x1 + (x3 >> 1)));
        block[1 + j] = (short)((x0 - x2) + ((x1 >> 1) - x3));
        block[2 + j] = (short)((x0 - x2) - ((x1 >> 1) - x3));
        block[3 + j] = (short)((x0 + x2) - (x1 + (x3 >> 1)));
      }//end for j...
    }//end if _q...
    else
    {
      for (j = 0; j < 16; j += 4)
      {
        /// 1st stage with pre-scaling and quantisation.
        int x0 = ((int)block[j] * _levelScale[_qm][j]) << _leftScale;
        int x1 = ((int)block[j + 1] * _levelScale[_qm][j + 1]) << _leftScale;
        int x2 = ((int)block[j + 2] * _levelScale[_qm][j + 2]) << _leftScale;
        int x3 = ((int)block[j + 3] * _levelScale[_qm][j + 3]) << _leftScale;

        /// 2nd stage.
        block[j] = (short)((x0 + x2) + (x1 + (x3 >> 1)));
        block[1 + j] = (short)((x0 - x2) + ((x1 >> 1) - x3));
        block[2 + j] = (short)((x0 - x2) - ((x1 >> 1) - x3));
        block[3 + j] = (short)((x0 + x2) - (x1 + (x3 >> 1)));
      }//end for j...
    }//end else...

		/// 1-D inverse IT in vert direction.
		for(j = 0; j < 4; j++)
		{
			/// 1st stage.
			int s0 = (int)block[j]					+ (int)block[j+8];
			int s1 = (int)block[j]					- (int)block[j+8];
			int s2 = ((int)block[j+4] >> 1)	- (int)block[j+12];
			int s3 = (int)block[j+4]				+ ((int)block[j+12] >> 1);

			/// 2nd stage.
			block[j]		= (short)((s0 + s3 + 32) >> 6);
			block[12+j]	= (short)((s0 - s3 + 32) >> 6);
			block[4+j]	= (short)((s1 + s2 + 32) >> 6);
			block[8+j]	= (short)((s1 - s2 + 32) >> 6);
		}//end for j...

  }//end if _mode == 0...
	else	///< _mode == 1 IT only.
	{
		/// 1-D inverse IT in horiz direction.
		for(j = 0; j < 16; j += 4)
		{
			/// 1st stage.
			int s0 = (int)block[j]					+ (int)block[2+j];
			int s1 = (int)block[j]					- (int)block[2+j];
			int s2 = (int)(block[1+j] >> 1)	- (int)block[3+j];
			int s3 = (int)block[1+j]				+ (int)(block[3+j] >> 1);

			/// 2nd stage.
			block[j]		= (short)(s0 + s3);
			block[3+j]	= (short)(s0 - s3);
			block[1+j]	= (short)(s1 + s2);
			block[2+j]	= (short)(s1 - s2);
		}//end for j...

		/// 1-D inverse IT in vert direction.
		for(j = 0; j < 4; j++)
		{
			/// 1st stage.
			int s0 = (int)block[j]					+ (int)block[8+j];
			int s1 = (int)block[j]					- (int)block[8+j];
			int s2 = (int)(block[4+j] >> 1)	- (int)block[12+j];
			int s3 = (int)block[4+j]				+ (int)(block[12+j] >> 1);

			/// 2nd stage.
			block[j]		= (short)((s0 + s3 + 32) >> 6);
			block[12+j]	= (short)((s0 - s3 + 32) >> 6);
			block[4+j]	= (short)((s1 + s2 + 32) >> 6);
			block[8+j]	= (short)((s1 - s2 + 32) >> 6);

		}//end for j...

	}//end else...

}//end InverseTransform.

/** Transfer inverse IT.
The inverse IT is performed on the coeffs and are written to 
the output.
@param pCoeff	: Input coeffs.
@param pOut		: Output data.
@return				:	none.
*/
void FastInverse4x4ITImpl1::InverseTransform(void* pCoeff, void* pOut)
{
	/// Copy to output and then do in-place inverse transform.
	memcpy(pOut, pCoeff, sizeof(short) * 16);
	InverseTransform(pOut);
}//end InverseTransform.

/** Set scaling array.
Each coefficient may be scaled before transforming and therefore 
requires setting up.
@param	pScale:	Scale factor array.
@return				:	none.
*/
void FastInverse4x4ITImpl1::SetScale(void* pScale)
{
	int		i,j,qm;
	int*	ptr = (int *)pScale;

	for(i = 0; i < 16; i++)
		_weightScale[i] = ptr[i];

	for(qm = 0; qm < 6; qm++)
		for(i = 0; i < 4; i++)
			for(j = 0; j < 4; j++)
			{
				int pos = 4*i + j;
				_levelScale[qm][pos] = NormAdjust[qm][ColSelector[pos]] * _weightScale[pos];
			}//end for qm & i & j...

}//end SetScale.


void* FastInverse4x4ITImpl1::GetScale(void)
{
	return( (void *)(_weightScale) );
}//end GetScale.

/** Set and get parameters for the implementation.
@param paramID	: Parameter to set/get.
@param paramVal	: Parameter value.
@return					: None (Set) or the param value (Get).
*/
void FastInverse4x4ITImpl1::SetParameter(int paramID, int paramVal)
{
	switch(paramID)
	{
		case QUANT_ID:
			{
				// TODO: Put these calcs into a table.
				if(paramVal != _q)	///< Do we need to change member values or keep previous?
				{
					_qm					= paramVal % 6;
					_qe					= paramVal/6;
					_f					= 1 << (3-_qe);
					_leftScale	= _qe-4;
					_rightScale	= 4-_qe;
					_q					= paramVal;
				}//end if q...
			}//end case QUANT_ID...
			break;
		default :
			/// Do nothing.
			break;
	}//end switch paramID...
}//end SetParameter.

int FastInverse4x4ITImpl1::GetParameter(int paramID)
{
	int ret;
	switch(paramID)
	{
		case QUANT_ID:
			ret = _q;
			break;
		default :
			ret = _q;	///< Quant value is returned as default.
			break;
	}//end switch paramID...

	return(ret);
}//end GetParameter.

///------------------------------- Multiple Data Methods -------------------------------------

/** In-place inverse Integer Transform.
The 2-D inverse IT is performed on the input coeffs and replaces them. A 1-D
inverse transform is performed on the rows first and then the cols.
@param ptr	: 4 Data sets to inverse transform.
@return			:	none.
*/
/*
void FastInverse4x4ITImpl1::InverseTransform(void* ptr1, void* ptr2, void* ptr3, void* ptr4)
{
  short* p1 = (short *)ptr1;
  short* p2 = (short *)ptr2;
  short* p3 = (short *)ptr3;
  short* p4 = (short *)ptr4;
  int j;

  if (_mode == TransformAndQuant)	///< 0 = IT&Q.
  {
    /// 1-D inverse IT in horiz direction.
    for (j = 0; j < 16; j += 4)
    {
      int x0, x1, x2, x3, w0, w1, w2, w3, y0, y1, y2, y3, z0, z1, z2, z3;
      /// 1st stage with pre-scaling and quantisation.
      if (_q < 24)
      {
        x0 = (((int)p1[j] * _levelScale[_qm][j]) + _f) >> _rightScale;
        w0 = (((int)p2[j] * _levelScale[_qm][j]) + _f) >> _rightScale;
        y0 = (((int)p3[j] * _levelScale[_qm][j]) + _f) >> _rightScale;
        z0 = (((int)p4[j] * _levelScale[_qm][j]) + _f) >> _rightScale;
        x1 = (((int)p1[j + 1] * _levelScale[_qm][j + 1]) + _f) >> _rightScale;
        w1 = (((int)p2[j + 1] * _levelScale[_qm][j + 1]) + _f) >> _rightScale;
        y1 = (((int)p3[j + 1] * _levelScale[_qm][j + 1]) + _f) >> _rightScale;
        z1 = (((int)p4[j + 1] * _levelScale[_qm][j + 1]) + _f) >> _rightScale;
        x2 = (((int)p1[j + 2] * _levelScale[_qm][j + 2]) + _f) >> _rightScale;
        w2 = (((int)p2[j + 2] * _levelScale[_qm][j + 2]) + _f) >> _rightScale;
        y2 = (((int)p3[j + 2] * _levelScale[_qm][j + 2]) + _f) >> _rightScale;
        z2 = (((int)p4[j + 2] * _levelScale[_qm][j + 2]) + _f) >> _rightScale;
        x3 = (((int)p1[j + 3] * _levelScale[_qm][j + 3]) + _f) >> _rightScale;
        w3 = (((int)p2[j + 3] * _levelScale[_qm][j + 3]) + _f) >> _rightScale;
        y3 = (((int)p3[j + 3] * _levelScale[_qm][j + 3]) + _f) >> _rightScale;
        z3 = (((int)p4[j + 3] * _levelScale[_qm][j + 3]) + _f) >> _rightScale;
      }//end if _q...
      else
      {
        x0 = ((int)p1[j] * _levelScale[_qm][j]) << _leftScale;
        w0 = ((int)p2[j] * _levelScale[_qm][j]) << _leftScale;
        y0 = ((int)p3[j] * _levelScale[_qm][j]) << _leftScale;
        z0 = ((int)p4[j] * _levelScale[_qm][j]) << _leftScale;
        x1 = ((int)p1[j + 1] * _levelScale[_qm][j + 1]) << _leftScale;
        w1 = ((int)p2[j + 1] * _levelScale[_qm][j + 1]) << _leftScale;
        y1 = ((int)p3[j + 1] * _levelScale[_qm][j + 1]) << _leftScale;
        z1 = ((int)p4[j + 1] * _levelScale[_qm][j + 1]) << _leftScale;
        x2 = ((int)p1[j + 2] * _levelScale[_qm][j + 2]) << _leftScale;
        w2 = ((int)p2[j + 2] * _levelScale[_qm][j + 2]) << _leftScale;
        y2 = ((int)p3[j + 2] * _levelScale[_qm][j + 2]) << _leftScale;
        z2 = ((int)p4[j + 2] * _levelScale[_qm][j + 2]) << _leftScale;
        x3 = ((int)p1[j + 3] * _levelScale[_qm][j + 3]) << _leftScale;
        w3 = ((int)p2[j + 3] * _levelScale[_qm][j + 3]) << _leftScale;
        y3 = ((int)p3[j + 3] * _levelScale[_qm][j + 3]) << _leftScale;
        z3 = ((int)p4[j + 3] * _levelScale[_qm][j + 3]) << _leftScale;
      }//end else...

      int s0 = x0 + x2; int s1 = x0 - x2;
      int r0 = w0 + w2; int r1 = w0 - w2;
      int t0 = y0 + y2; int t1 = y0 - y2;
      int u0 = z0 + z2; int u1 = z0 - z2;
      int s2 = (x1 >> 1) - x3; int s3 = x1 + (x3 >> 1);
      int r2 = (w1 >> 1) - w3; int r3 = w1 + (w3 >> 1);
      int t2 = (y1 >> 1) - y3; int t3 = y1 + (y3 >> 1);
      int u2 = (z1 >> 1) - z3; int u3 = z1 + (z3 >> 1);

      /// 2nd stage.
      p1[j] = (short)(s0 + s3); p1[3 + j] = (short)(s0 - s3);
      p2[j] = (short)(r0 + r3); p2[3 + j] = (short)(r0 - r3);
      p3[j] = (short)(t0 + t3); p3[3 + j] = (short)(t0 - t3);
      p4[j] = (short)(u0 + u3); p4[3 + j] = (short)(u0 - u3);
      p1[1 + j] = (short)(s1 + s2); p1[2 + j] = (short)(s1 - s2);
      p2[1 + j] = (short)(r1 + r2); p2[2 + j] = (short)(r1 - r2);
      p3[1 + j] = (short)(t1 + t2); p3[2 + j] = (short)(t1 - t2);
      p4[1 + j] = (short)(u1 + u2); p4[2 + j] = (short)(u1 - u2);
    }//end for j...

     /// 1-D inverse IT in vert direction.
    for (j = 0; j < 4; j++)
    {
      /// 1st stage.
      int s0 = (int)p1[j] + (int)p1[j + 8]; int s1 = (int)p1[j] - (int)p1[j + 8];
      int t0 = (int)p2[j] + (int)p2[j + 8]; int t1 = (int)p2[j] - (int)p2[j + 8];
      int u0 = (int)p3[j] + (int)p3[j + 8]; int u1 = (int)p3[j] - (int)p3[j + 8];
      int v0 = (int)p4[j] + (int)p4[j + 8]; int v1 = (int)p4[j] - (int)p4[j + 8];
      int s2 = ((int)p1[j + 4] >> 1) - (int)p1[j + 12]; int s3 = (int)p1[j + 4] + ((int)p1[j + 12] >> 1);
      int t2 = ((int)p2[j + 4] >> 1) - (int)p2[j + 12]; int t3 = (int)p2[j + 4] + ((int)p2[j + 12] >> 1);
      int u2 = ((int)p3[j + 4] >> 1) - (int)p3[j + 12]; int u3 = (int)p3[j + 4] + ((int)p3[j + 12] >> 1);
      int v2 = ((int)p4[j + 4] >> 1) - (int)p4[j + 12]; int v3 = (int)p4[j + 4] + ((int)p4[j + 12] >> 1);

      /// 2nd stage.
      p1[j] = (short)((s0 + s3 + 32) >> 6); p1[12 + j] = (short)((s0 - s3 + 32) >> 6);
      p2[j] = (short)((t0 + t3 + 32) >> 6); p2[12 + j] = (short)((t0 - t3 + 32) >> 6);
      p3[j] = (short)((u0 + u3 + 32) >> 6); p3[12 + j] = (short)((u0 - u3 + 32) >> 6);
      p4[j] = (short)((v0 + v3 + 32) >> 6); p4[12 + j] = (short)((v0 - v3 + 32) >> 6);
      p1[4 + j] = (short)((s1 + s2 + 32) >> 6); p1[8 + j] = (short)((s1 - s2 + 32) >> 6);
      p2[4 + j] = (short)((t1 + t2 + 32) >> 6); p2[8 + j] = (short)((t1 - t2 + 32) >> 6);
      p3[4 + j] = (short)((u1 + u2 + 32) >> 6); p3[8 + j] = (short)((u1 - u2 + 32) >> 6);
      p4[4 + j] = (short)((v1 + v2 + 32) >> 6); p4[8 + j] = (short)((v1 - v2 + 32) >> 6);
    }//end for j...

  }//end if _mode == 0...
  else if (_mode == QuantOnly)	///< 2 = Q only.
  {
    /// Pre-scaling and quantisation are combined.

    if (_q < 24)
    {
      for (j = 0; j < 16; j++)
      {
        p1[j] = (short)((((int)p1[j] * _levelScale[_qm][j]) + _f) >> _rightScale);
        p2[j] = (short)((((int)p2[j] * _levelScale[_qm][j]) + _f) >> _rightScale);
        p3[j] = (short)((((int)p3[j] * _levelScale[_qm][j]) + _f) >> _rightScale);
        p4[j] = (short)((((int)p4[j] * _levelScale[_qm][j]) + _f) >> _rightScale);
      }//end for j...
    }//end if _q...
    else
    {
      for (j = 0; j < 16; j++)
      {
        p1[j] = (short)(((int)p1[j] * _levelScale[_qm][j]) << _leftScale);
        p2[j] = (short)(((int)p2[j] * _levelScale[_qm][j]) << _leftScale);
        p3[j] = (short)(((int)p3[j] * _levelScale[_qm][j]) << _leftScale);
        p4[j] = (short)(((int)p4[j] * _levelScale[_qm][j]) << _leftScale);
      }//end for j...
    }//end else...
  }//end else if _mode == 2...
  else	///< _mode == 1 IT only.
  {
    /// 1-D inverse IT in horiz direction.
    for (j = 0; j < 16; j += 4)
    {
      /// 1st stage.
      int s0 = (int)p1[j] + (int)p1[2 + j];
      int s1 = (int)p1[j] - (int)p1[2 + j];
      int t0 = (int)p2[j] + (int)p2[2 + j];
      int t1 = (int)p2[j] - (int)p2[2 + j];
      int u0 = (int)p3[j] + (int)p3[2 + j];
      int u1 = (int)p3[j] - (int)p3[2 + j];
      int v0 = (int)p4[j] + (int)p4[2 + j];
      int v1 = (int)p4[j] - (int)p4[2 + j];
      int s2 = (int)(p1[1 + j] >> 1) - (int)p1[3 + j];
      int s3 = (int)p1[1 + j] + (int)(p1[3 + j] >> 1);
      int t2 = (int)(p2[1 + j] >> 1) - (int)p2[3 + j];
      int t3 = (int)p2[1 + j] + (int)(p2[3 + j] >> 1);
      int u2 = (int)(p3[1 + j] >> 1) - (int)p3[3 + j];
      int u3 = (int)p3[1 + j] + (int)(p3[3 + j] >> 1);
      int v2 = (int)(p4[1 + j] >> 1) - (int)p4[3 + j];
      int v3 = (int)p4[1 + j] + (int)(p4[3 + j] >> 1);

      /// 2nd stage.
      p1[j] = (short)(s0 + s3);
      p1[3 + j] = (short)(s0 - s3);
      p2[j] = (short)(t0 + t3);
      p2[3 + j] = (short)(t0 - t3);
      p3[j] = (short)(u0 + u3);
      p3[3 + j] = (short)(u0 - u3);
      p4[j] = (short)(v0 + v3);
      p4[3 + j] = (short)(v0 - v3);
      p1[1 + j] = (short)(s1 + s2);
      p1[2 + j] = (short)(s1 - s2);
      p2[1 + j] = (short)(t1 + t2);
      p2[2 + j] = (short)(t1 - t2);
      p3[1 + j] = (short)(u1 + u2);
      p3[2 + j] = (short)(u1 - u2);
      p4[1 + j] = (short)(v1 + v2);
      p4[2 + j] = (short)(v1 - v2);

    }//end for j...

     /// 1-D inverse IT in vert direction.
    for (j = 0; j < 4; j++)
    {
      /// 1st stage.
      int s0 = (int)p1[j] + (int)p1[8 + j];
      int s1 = (int)p1[j] - (int)p1[8 + j];
      int t0 = (int)p2[j] + (int)p2[8 + j];
      int t1 = (int)p2[j] - (int)p2[8 + j];
      int u0 = (int)p3[j] + (int)p3[8 + j];
      int u1 = (int)p3[j] - (int)p3[8 + j];
      int v0 = (int)p4[j] + (int)p4[8 + j];
      int v1 = (int)p4[j] - (int)p4[8 + j];
      int s2 = (int)(p1[4 + j] >> 1) - (int)p1[12 + j];
      int s3 = (int)p1[4 + j] + (int)(p1[12 + j] >> 1);
      int t2 = (int)(p2[4 + j] >> 1) - (int)p2[12 + j];
      int t3 = (int)p2[4 + j] + (int)(p2[12 + j] >> 1);
      int u2 = (int)(p3[4 + j] >> 1) - (int)p3[12 + j];
      int u3 = (int)p3[4 + j] + (int)(p3[12 + j] >> 1);
      int v2 = (int)(p4[4 + j] >> 1) - (int)p4[12 + j];
      int v3 = (int)p4[4 + j] + (int)(p4[12 + j] >> 1);

      /// 2nd stage.
      p1[j] = (short)((s0 + s3 + 32) >> 6);
      p1[12 + j] = (short)((s0 - s3 + 32) >> 6);
      p2[j] = (short)((t0 + t3 + 32) >> 6);
      p2[12 + j] = (short)((t0 - t3 + 32) >> 6);
      p3[j] = (short)((u0 + u3 + 32) >> 6);
      p3[12 + j] = (short)((u0 - u3 + 32) >> 6);
      p4[j] = (short)((v0 + v3 + 32) >> 6);
      p4[12 + j] = (short)((v0 - v3 + 32) >> 6);
      p1[4 + j] = (short)((s1 + s2 + 32) >> 6);
      p1[8 + j] = (short)((s1 - s2 + 32) >> 6);
      p2[4 + j] = (short)((t1 + t2 + 32) >> 6);
      p2[8 + j] = (short)((t1 - t2 + 32) >> 6);
      p3[4 + j] = (short)((u1 + u2 + 32) >> 6);
      p3[8 + j] = (short)((u1 - u2 + 32) >> 6);
      p4[4 + j] = (short)((v1 + v2 + 32) >> 6);
      p4[8 + j] = (short)((v1 - v2 + 32) >> 6);

    }//end for j...

  }//end else...

}//end InverseTransform.
*/