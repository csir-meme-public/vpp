/** @file

MODULE				: FastForward4x4On8x8ITImpl1

TAG						: FF4O8ITI1

FILE NAME			: FastForward4x4On8x8ITImpl1.cpp

DESCRIPTION		: A class to implement a fast forward 4x4 2-D integer
                transform defined by the H.264 standard on the 8x8 chr
                macroblock input. The memory layout for this implementation
                is a 256 length contiguous linear array. It implements the
                IForwardTransform interface. The scaling	is part of the
                quantisation process.

COPYRIGHT			: (c)CSIR 2007-2020 all rights resevered

LICENSE				: Software License Agreement (BSD License)

RESTRICTIONS	: Redistribution and use in source and binary forms, with or without 
								modification, are permitted provided that the following conditions 
								are met:

								* Redistributions of source code must retain the above copyright notice, 
								this list of conditions and the following disclaimer.
								* Redistributions in binary form must reproduce the above copyright notice, 
								this list of conditions and the following disclaimer in the documentation 
								and/or other materials provided with the distribution.
								* Neither the name of the CSIR nor the names of its contributors may be used 
								to endorse or promote products derived from this software without specific 
								prior written permission.

								THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
								"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
								LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
								A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
								CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
								EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
								PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
								PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
								LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
								NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
								SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

======================================================================================
*/
#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#else
#include <stdio.h>
#endif

#include <string.h>
#include "FastForward4x4On8x8ITImpl1.h"

//#define FF4O8ITI1_UNROLL_LOOPS_AND_INTERLEAVE

/*
---------------------------------------------------------------------------
	Constants.
---------------------------------------------------------------------------
*/
const int FastForward4x4On8x8ITImpl1::NormAdjust[6][3] =
{
	{13107, 5243, 8066 },
	{11916, 4660, 7490 },
	{10082, 4194, 6554 },
	{ 9362, 3647, 5825 },
	{ 8192, 3355, 5243 },
	{ 7282, 2893, 4559 }
};

const int FastForward4x4On8x8ITImpl1::ColSelector[16] =
{
	0, 2, 0, 2 ,
	2, 1, 2, 1 ,
	0, 2, 0, 2 ,
	2, 1, 2, 1 
};

/*
---------------------------------------------------------------------------
	Construction.
---------------------------------------------------------------------------
*/
FastForward4x4On8x8ITImpl1::FastForward4x4On8x8ITImpl1()	
{ 
	_mode		= TransformAndQuant; 
	_intra	= 1;
	_q			= 1;
	_qm			= _q % 6;
	_qe			= _q/6;
	if(_intra)
		_f = (1 << (15+_qe))/3;
	else
		_f = (1 << (15+_qe))/6;
	_scale	= 15+_qe;

}//end constructor.

/*
---------------------------------------------------------------------------
	Interface Methods.
---------------------------------------------------------------------------
*/
/** In-place forward Integer Transform.
The 2-D IT is performed on the input and replaces it with the coeffs. A 1-D
transform is performed on the rows first and then the cols.
@param ptr	: Data to transform.
@return			:	none.
*/
#ifdef FF4O16ITI2_UNROLL_LOOPS_AND_INTERLEAVE


#else ///< !FF4O8ITI1_UNROLL_LOOPS_AND_INTERLEAVE

void FastForward4x4On8x8ITImpl1::Transform(void* ptr)
{
  /// The mem format of the mb 8x8 with the 4x4 blks is linear contiguous.
  /// pmb = { blk[0][0][0...16], blk[0][1][0...16], ... , blk[1][1][0...16] }.
  short* pb1 = (short *)ptr;
  short* pb2 = &(pb1[16]);
  short* pb3 = &(pb1[32]);
  short* pb4 = &(pb1[48]);
  int i,j;

  if (_mode == IForwardTransform::QuantOnly)
  {
    /// Scaling and quantisation are combined.
    const int* pNorm = NormAdjust[_qm];

    for (j = 0; j < 16; j++)  ///< 4 blks in the mb with linear mem addressing.
    {
      const int norm = pNorm[ColSelector[j]];

      if (*pb1 < 0) *pb1++ = (short)(-((((-(int)(*pb1)) * norm) + _f) >> _scale));
      else          *pb1++ = (short)((((int)(*pb1) * norm) + _f) >> _scale);

      if (*pb2 < 0) *pb2++ = (short)(-((((-(int)(*pb2)) * norm) + _f) >> _scale));
      else          *pb2++ = (short)((((int)(*pb2) * norm) + _f) >> _scale);

      if (*pb3 < 0) *pb3++ = (short)(-((((-(int)(*pb3)) * norm) + _f) >> _scale));
      else          *pb3++ = (short)((((int)(*pb3) * norm) + _f) >> _scale);

      if (*pb4 < 0) *pb4++ = (short)(-((((-(int)(*pb4)) * norm) + _f) >> _scale));
      else          *pb4++ = (short)((((int)(*pb4) * norm) + _f) >> _scale);
    }//end for j...
  }//end if QuantOnly...
  else if(_mode == IForwardTransform::TransformOnly)
  {
    /// 1-D forward horiz direction.
    for (j = 0; j < 16; j += 4) ///< 1 blk row at a time.
    {
      ///... 1st 2 blks
      /// 1st stage transform.
      int s0 = (int)(pb1[j] + pb1[j + 3]);
      int s3 = (int)(pb1[j] - pb1[j + 3]);
      int s1 = (int)(pb1[j + 1] + pb1[j + 2]);
      int s2 = (int)(pb1[j + 1] - pb1[j + 2]);
      int t0 = (int)(pb2[j] + pb2[j + 3]);
      int t3 = (int)(pb2[j] - pb2[j + 3]);
      int t1 = (int)(pb2[j + 1] + pb2[j + 2]);
      int t2 = (int)(pb2[j + 1] - pb2[j + 2]);
      /// 2nd stage transform.
      pb1[j]     = (short)(s0 + s1);
      pb1[j + 2] = (short)(s0 - s1);
      pb1[j + 1] = (short)(s2 + (s3 << 1));
      pb1[j + 3] = (short)(s3 - (s2 << 1));
      pb2[j]     = (short)(t0 + t1);
      pb2[j + 2] = (short)(t0 - t1);
      pb2[j + 1] = (short)(t2 + (t3 << 1));
      pb2[j + 3] = (short)(t3 - (t2 << 1));

      ///... 2nd 2 blks
      /// 1st stage transform.
      s0 = (int)(pb3[j] + pb3[j + 3]);
      s3 = (int)(pb3[j] - pb3[j + 3]);
      s1 = (int)(pb3[j + 1] + pb3[j + 2]);
      s2 = (int)(pb3[j + 1] - pb3[j + 2]);
      t0 = (int)(pb4[j] + pb4[j + 3]);
      t3 = (int)(pb4[j] - pb4[j + 3]);
      t1 = (int)(pb4[j + 1] + pb4[j + 2]);
      t2 = (int)(pb4[j + 1] - pb4[j + 2]);
      /// 2nd stage transform.
      pb3[j]     = (short)(s0 + s1);
      pb3[j + 2] = (short)(s0 - s1);
      pb3[j + 1] = (short)(s2 + (s3 << 1));
      pb3[j + 3] = (short)(s3 - (s2 << 1));
      pb4[j]     = (short)(t0 + t1);
      pb4[j + 2] = (short)(t0 - t1);
      pb4[j + 1] = (short)(t2 + (t3 << 1));
      pb4[j + 3] = (short)(t3 - (t2 << 1));
    }//end for j...

    /// 1-D forward vert direction.
    for (j = 0; j < 4; j++) /// 1 blk col at a time.
    {
      ///... 1st 2 blks
      /// 1st stage transform.
      int s0 = (int)(pb1[j] + pb1[j + 12]);
      int s3 = (int)(pb1[j] - pb1[j + 12]);
      int s1 = (int)(pb1[j + 4] + pb1[j + 8]);
      int s2 = (int)(pb1[j + 4] - pb1[j + 8]);
      int t0 = (int)(pb2[j] + pb2[j + 12]);
      int t3 = (int)(pb2[j] - pb2[j + 12]);
      int t1 = (int)(pb2[j + 4] + pb2[j + 8]);
      int t2 = (int)(pb2[j + 4] - pb2[j + 8]);
      /// 2nd stage transform.
      pb1[j]      = (short)(s0 + s1);
      pb1[j + 8]  = (short)(s0 - s1);
      pb1[j + 4]  = (short)(s2 + (s3 << 1));
      pb1[j + 12] = (short)(s3 - (s2 << 1));
      pb2[j]      = (short)(t0 + t1);
      pb2[j + 8]  = (short)(t0 - t1);
      pb2[j + 4]  = (short)(t2 + (t3 << 1));
      pb2[j + 12] = (short)(t3 - (t2 << 1));

      ///... 2nd 2 blks
      /// 1st stage transform.
      s0 = (int)(pb3[j] + pb3[j + 12]);
      s3 = (int)(pb3[j] - pb3[j + 12]);
      s1 = (int)(pb3[j + 4] + pb3[j + 8]);
      s2 = (int)(pb3[j + 4] - pb3[j + 8]);
      t0 = (int)(pb4[j] + pb4[j + 12]);
      t3 = (int)(pb4[j] - pb4[j + 12]);
      t1 = (int)(pb4[j + 4] + pb4[j + 8]);
      t2 = (int)(pb4[j + 4] - pb4[j + 8]);
      /// 2nd stage transform.
      pb3[j]      = (short)(s0 + s1);
      pb3[j + 8]  = (short)(s0 - s1);
      pb3[j + 4]  = (short)(s2 + (s3 << 1));
      pb3[j + 12] = (short)(s3 - (s2 << 1));
      pb4[j]      = (short)(t0 + t1);
      pb4[j + 8]  = (short)(t0 - t1);
      pb4[j + 4]  = (short)(t2 + (t3 << 1));
      pb4[j + 12] = (short)(t3 - (t2 << 1));
    }//end for j...

  }//end else if TransformOnly...
  else ///< if _mode == TransformAndQuant)
  {
    short* pb = pb1;
    const int* pNorm = NormAdjust[_qm];

    for (i = 0; i < 4; i++, pb += 16) ///< 4 blks in the mb with linear mem addressing.
    {
      /// 1-D forward horiz direction.
      for (j = 0; j < 16; j += 4)
      {
        /// 1st stage transform.
        int s0 = (int)(pb[j] + pb[j + 3]);
        int s3 = (int)(pb[j] - pb[j + 3]);
        int s1 = (int)(pb[j + 1] + pb[j + 2]);
        int s2 = (int)(pb[j + 1] - pb[j + 2]);

        /// 2nd stage transform.
        pb[j]     = (short)(s0 + s1);
        pb[j + 2] = (short)(s0 - s1);
        pb[j + 1] = (short)(s2 + (s3 << 1));
        pb[j + 3] = (short)(s3 - (s2 << 1));
      }//end for j...

      /// 1-D forward vert direction.
      for (j = 0; j < 4; j++)
      {
        /// 1st stage transform.
        int s0 = (int)(pb[j] + pb[j + 12]);
        int s3 = (int)(pb[j] - pb[j + 12]);
        int s1 = (int)(pb[j + 4] + pb[j + 8]);
        int s2 = (int)(pb[j + 4] - pb[j + 8]);

        /// 2nd stage transform with scaling and quantisation.
        int w0 = s0 + s1;
        int w2 = s0 - s1;
        if (w0 < 0) pb[j] = (short)(-((((-w0) * pNorm[ColSelector[j]]) + _f) >> _scale));
        else        pb[j] = (short)(((w0 * pNorm[ColSelector[j]]) + _f) >> _scale);
        if (w2 < 0) pb[j + 8] = (short)(-((((-w2) * pNorm[ColSelector[j + 8]]) + _f) >> _scale));
        else        pb[j + 8] = (short)(((w2 * pNorm[ColSelector[j + 8]]) + _f) >> _scale);
        int w1 = s2 + (s3 << 1);
        int w3 = s3 - (s2 << 1);
        if (w1 < 0) pb[j + 4] = (short)(-((((-w1) * pNorm[ColSelector[j + 4]]) + _f) >> _scale));
        else        pb[j + 4] = (short)(((w1 * pNorm[ColSelector[j + 4]]) + _f) >> _scale);
        if (w3 < 0) pb[j + 12] = (short)(-((((-w3) * pNorm[ColSelector[j + 12]]) + _f) >> _scale));
        else        pb[j + 12] = (short)(((w3 * pNorm[ColSelector[j + 12]]) + _f) >> _scale);
      }//end for j...
    }//end for i...
  }//end else TransformAndQuant...

}//end Transform.

#endif 

/** Transfer forward IT.
The IT is performed on the input and the coeffs are written to 
the output.
@param pIn		: Input data.
@param pCoeff	: Output coeffs.
@return				:	none.
*/
void FastForward4x4On8x8ITImpl1::Transform(void* pIn, void* pCoeff)
{
	/// Copy to output and then do in-place inverse transform.
	memcpy(pCoeff, pIn, sizeof(short) * 64);
	Transform(pCoeff);
}//end Transform.

/** Set and get parameters for the implementation.
An Intra and Inter parameter and quantisation parameter are the only 
requirement for this implementation.
@param paramID	: Parameter to set/get.
@param paramVal	: Parameter value.
@return					: None (Set) or the param value (Get).
*/
void FastForward4x4On8x8ITImpl1::SetParameter(int paramID, int paramVal)
{
	switch(paramID)
	{
		case QUANT_ID:
			{
				// TODO: Put these calcs into a table.
				if(paramVal != _q)	///< Do we need to change member values or keep previous?
				{
					_q = paramVal;
          SetNewQP();
				}//end if q...
			}//end case QUANT_ID...
			break;
		case INTRA_FLAG_ID:
			_intra = paramVal;
      SetNewQP();
			break;
		default :
			/// Do nothing.
			break;
	}//end switch paramID...
}//end SetParameter.

int FastForward4x4On8x8ITImpl1::GetParameter(int paramID)
{
	int ret;
	switch(paramID)
	{
		case QUANT_ID:
			ret = _q;
			break;
		case INTRA_FLAG_ID:
			ret = _intra;
			break;
		default :
			ret = _intra;	///< Intra flag is returned as default.
			break;
	}//end switch paramID...

	return(ret);
}//end GetParameter.

/** Set internal quant members based on _q.
@return: none.
*/
void FastForward4x4On8x8ITImpl1::SetNewQP(void)
{
	_qm			= _q % 6;
	_qe			= _q/6;
	if(_intra)
		_f = (1 << (15+_qe))/3;
	else
		_f = (1 << (15+_qe))/6;
	_scale	= 15+_qe;
}//end SetNewQP.
