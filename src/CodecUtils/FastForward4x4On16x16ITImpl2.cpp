/** @file

MODULE				: FastForward4x4On16x16ITImpl2

TAG						: FF4O16ITI2

FILE NAME			: FastForward4x4On16x16ITImpl2.cpp

DESCRIPTION		: A class to implement a fast forward 4x4 2-D integer
                transform defined by the H.264 standard on the 16x16
                macroblock input. The memory layout for this implementation
                is a 256 length contiguous linear array. It implements the
                IForwardTransform interface. The scaling	is part of the
                quantisation process.

COPYRIGHT			: (c)CSIR 2007-2020 all rights resevered

LICENSE				: Software License Agreement (BSD License)

RESTRICTIONS	: Redistribution and use in source and binary forms, with or without 
								modification, are permitted provided that the following conditions 
								are met:

								* Redistributions of source code must retain the above copyright notice, 
								this list of conditions and the following disclaimer.
								* Redistributions in binary form must reproduce the above copyright notice, 
								this list of conditions and the following disclaimer in the documentation 
								and/or other materials provided with the distribution.
								* Neither the name of the CSIR nor the names of its contributors may be used 
								to endorse or promote products derived from this software without specific 
								prior written permission.

								THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
								"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
								LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
								A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
								CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
								EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
								PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
								PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
								LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
								NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
								SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

======================================================================================
*/
#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#else
#include <stdio.h>
#endif

#include <string.h>
#include "FastForward4x4On16x16ITImpl2.h"

//#define FF4O16ITI2_UNROLL_LOOPS_AND_INTERLEAVE

/*
---------------------------------------------------------------------------
	Constants.
---------------------------------------------------------------------------
*/
const int FastForward4x4On16x16ITImpl2::NormAdjust[6][3] =
{
	{13107, 5243, 8066 },
	{11916, 4660, 7490 },
	{10082, 4194, 6554 },
	{ 9362, 3647, 5825 },
	{ 8192, 3355, 5243 },
	{ 7282, 2893, 4559 }
};

const int FastForward4x4On16x16ITImpl2::ColSelector[16] =
{
	0, 2, 0, 2 ,
	2, 1, 2, 1 ,
	0, 2, 0, 2 ,
	2, 1, 2, 1 
};

/*
---------------------------------------------------------------------------
	Construction.
---------------------------------------------------------------------------
*/
FastForward4x4On16x16ITImpl2::FastForward4x4On16x16ITImpl2()	
{ 
	_mode		= TransformAndQuant; 
	_intra	= 1;
	_q			= 1;
	_qm			= _q % 6;
	_qe			= _q/6;
	if(_intra)
		_f = (1 << (15+_qe))/3;
	else
		_f = (1 << (15+_qe))/6;
	_scale	= 15+_qe;

}//end constructor.

/*
---------------------------------------------------------------------------
	Interface Methods.
---------------------------------------------------------------------------
*/
/** In-place forward Integer Transform.
The 2-D IT is performed on the input and replaces it with the coeffs. A 1-D
transform is performed on the rows first and then the cols.
@param ptr	: Data to transform.
@return			:	none.
*/
#ifdef FF4O16ITI2_UNROLL_LOOPS_AND_INTERLEAVE

void FastForward4x4On16x16ITImpl2::Transform(void* ptr)
{
	short* block = (short *)ptr;

	if(_mode == IForwardTransform::TransformOnly)
	{
		register int s0,s1,s2,s3;
		register int v0,v1,v2,v3;
		register int r0,r1,r2,r3;
		register int u0,u1,u2,u3;

		/// 1-D forward horiz direction.

		/// 1st stage transform.
		s0 = (int)(block[0]	 + block[3]);
		s3 = (int)(block[0]	 - block[3]);
		s1 = (int)(block[1]  + block[2]);
		s2 = (int)(block[1]  - block[2]);
		v0 = (int)(block[4]	 + block[7]);
		v3 = (int)(block[4]	 - block[7]);
		v1 = (int)(block[5]  + block[6]);
		v2 = (int)(block[5]  - block[6]);
		r0 = (int)(block[8]	 + block[11]);
		r3 = (int)(block[8]	 - block[11]);
		r1 = (int)(block[9]  + block[10]);
		r2 = (int)(block[9]  - block[10]);
		u0 = (int)(block[12] + block[15]);
		u3 = (int)(block[12] - block[15]);
		u1 = (int)(block[13] + block[14]);
		u2 = (int)(block[13] - block[14]);

		/// 2nd stage transform.
		block[0]	= (short)(s0 + s1);
		block[1]	= (short)(s2 + (s3 << 1));
		block[2]	= (short)(s0 - s1);
		block[3]	= (short)(s3 - (s2 << 1));
		block[4]	= (short)(v0 + v1);
		block[5]	= (short)(v2 + (v3 << 1));
		block[6]	= (short)(v0 - v1);
		block[7]	= (short)(v3 - (v2 << 1));
		block[8]	= (short)(r0 + r1);
		block[9]	= (short)(r2 + (r3 << 1));
		block[10]	= (short)(r0 - r1);
		block[11]	= (short)(r3 - (r2 << 1));
		block[12]	= (short)(u0 + u1);
		block[13]	= (short)(u2 + (u3 << 1));
		block[14]	= (short)(u0 - u1);
		block[15]	= (short)(u3 - (u2 << 1));

		/// 1-D forward vert direction.

		/// 1st stage transform.
		s0 = (int)(block[0]	+ block[12]);
		s3 = (int)(block[0]	- block[12]);
		v0 = (int)(block[1]	+ block[13]);
		v3 = (int)(block[1]	- block[13]);
		r0 = (int)(block[2]	+ block[14]);
		r3 = (int)(block[2]	- block[14]);
		u0 = (int)(block[3]	+ block[15]);
		u3 = (int)(block[3]	- block[15]);
		s1 = (int)(block[4] + block[8]);
		s2 = (int)(block[4] - block[8]);
		v1 = (int)(block[5] + block[9]);
		v2 = (int)(block[5] - block[9]);
		r1 = (int)(block[6] + block[10]);
		r2 = (int)(block[6] - block[10]);
		u1 = (int)(block[7] + block[11]);
		u2 = (int)(block[7] - block[11]);

		/// 2nd stage transform.
		block[0]	= (short)(s0 + s1);
		block[1]	= (short)(v0 + v1);
		block[2]	= (short)(r0 + r1);
		block[3]	= (short)(u0 + u1);
		block[4]	= (short)(s2 + (s3 << 1));
		block[5]	= (short)(v2 + (v3 << 1));
		block[6]	= (short)(r2 + (r3 << 1));
		block[7]	= (short)(u2 + (u3 << 1));
		block[8]	= (short)(s0 - s1);
		block[9]	= (short)(v0 - v1);
		block[10]	= (short)(r0 - r1);
		block[11]	= (short)(u0 - u1);
		block[12]	= (short)(s3 - (s2 << 1));
		block[13]	= (short)(v3 - (v2 << 1));
		block[14]	= (short)(r3 - (r2 << 1));
		block[15]	= (short)(u3 - (u2 << 1));

	}//end if TransformOnly...
	else if(_mode == IForwardTransform::QuantOnly)
	{
		/// Scaling and quantisation are combined.
		const int* pNorm = NormAdjust[_qm];

		/// [0]
		if( block[0] >= 0 )
			block[0] = (short)( (((int)block[0] * pNorm[0]) + _f) >> _scale );
		else
			block[0] = (short)(-( (((-(int)block[0]) * pNorm[0]) + _f) >> _scale ));
		/// [1]
		if( block[1] >= 0 )
			block[1] = (short)( (((int)block[1] * pNorm[2]) + _f) >> _scale );
		else
			block[1] = (short)(-( (((-(int)block[1]) * pNorm[2]) + _f) >> _scale ));
		/// [2]
		if( block[2] >= 0 )
			block[2] = (short)( (((int)block[2] * pNorm[0]) + _f) >> _scale );
		else
			block[2] = (short)(-( (((-(int)block[2]) * pNorm[0]) + _f) >> _scale ));
		/// [3]
		if( block[3] >= 0 )
			block[3] = (short)( (((int)block[3] * pNorm[2]) + _f) >> _scale );
		else
			block[3] = (short)(-( (((-(int)block[3]) * pNorm[2]) + _f) >> _scale ));
		/// [4]
		if( block[4] >= 0 )
			block[4] = (short)( (((int)block[4] * pNorm[2]) + _f) >> _scale );
		else
			block[4] = (short)(-( (((-(int)block[4]) * pNorm[2]) + _f) >> _scale ));
		/// [5]
		if( block[5] >= 0 )
			block[5] = (short)( (((int)block[5] * pNorm[1]) + _f) >> _scale );
		else
			block[5] = (short)(-( (((-(int)block[5]) * pNorm[1]) + _f) >> _scale ));
		/// [6]
		if( block[6] >= 0 )
			block[6] = (short)( (((int)block[6] * pNorm[2]) + _f) >> _scale );
		else
			block[6] = (short)(-( (((-(int)block[6]) * pNorm[2]) + _f) >> _scale ));
		/// [7]
		if( block[7] >= 0 )
			block[7] = (short)( (((int)block[7] * pNorm[1]) + _f) >> _scale );
		else
			block[7] = (short)(-( (((-(int)block[7]) * pNorm[1]) + _f) >> _scale ));
		/// [8]
		if( block[8] >= 0 )
			block[8] = (short)( (((int)block[8] * pNorm[0]) + _f) >> _scale );
		else
			block[8] = (short)(-( (((-(int)block[8]) * pNorm[0]) + _f) >> _scale ));
		/// [9]
		if( block[9] >= 0 )
			block[9] = (short)( (((int)block[9] * pNorm[2]) + _f) >> _scale );
		else
			block[9] = (short)(-( (((-(int)block[9]) * pNorm[2]) + _f) >> _scale ));
		/// [10]
		if( block[10] >= 0 )
			block[10] = (short)( (((int)block[10] * pNorm[0]) + _f) >> _scale );
		else
			block[10] = (short)(-( (((-(int)block[10]) * pNorm[0]) + _f) >> _scale ));
		/// [11]
		if( block[11] >= 0 )
			block[11] = (short)( (((int)block[11] * pNorm[2]) + _f) >> _scale );
		else
			block[11] = (short)(-( (((-(int)block[11]) * pNorm[2]) + _f) >> _scale ));
		/// [12]
		if( block[12] >= 0 )
			block[12] = (short)( (((int)block[12] * pNorm[2]) + _f) >> _scale );
		else
			block[12] = (short)(-( (((-(int)block[12]) * pNorm[2]) + _f) >> _scale ));
		/// [13]
		if( block[13] >= 0 )
			block[13] = (short)( (((int)block[13] * pNorm[1]) + _f) >> _scale );
		else
			block[13] = (short)(-( (((-(int)block[13]) * pNorm[1]) + _f) >> _scale ));
		/// [14]
		if( block[14] >= 0 )
			block[14] = (short)( (((int)block[14] * pNorm[2]) + _f) >> _scale );
		else
			block[14] = (short)(-( (((-(int)block[14]) * pNorm[2]) + _f) >> _scale ));
		/// [15]
		if( block[15] >= 0 )
			block[15] = (short)( (((int)block[15] * pNorm[1]) + _f) >> _scale );
		else
			block[15] = (short)(-( (((-(int)block[15]) * pNorm[1]) + _f) >> _scale ));

	}//end if QuantOnly...
	else ///< if (_mode == IForwardTransform::TransformAndQuant)
	{
		register int s0,s1,s2,s3;
		register int v0,v1,v2,v3;
		register int r0,r1,r2,r3;
		register int u0,u1,u2,u3;

		/// 1-D forward horiz direction.

		/// 1st stage transform.
		s0 = (int)(block[0]	 + block[3]);
		s3 = (int)(block[0]	 - block[3]);
		s1 = (int)(block[1]  + block[2]);
		s2 = (int)(block[1]  - block[2]);
		v0 = (int)(block[4]	 + block[7]);
		v3 = (int)(block[4]	 - block[7]);
		v1 = (int)(block[5]  + block[6]);
		v2 = (int)(block[5]  - block[6]);
		r0 = (int)(block[8]	 + block[11]);
		r3 = (int)(block[8]	 - block[11]);
		r1 = (int)(block[9]  + block[10]);
		r2 = (int)(block[9]  - block[10]);
		u0 = (int)(block[12] + block[15]);
		u3 = (int)(block[12] - block[15]);
		u1 = (int)(block[13] + block[14]);
		u2 = (int)(block[13] - block[14]);

		/// 2nd stage transform.
		block[0]	= (short)(s0 + s1);
		block[1]	= (short)(s2 + (s3 << 1));
		block[2]	= (short)(s0 - s1);
		block[3]	= (short)(s3 - (s2 << 1));
		block[4]	= (short)(v0 + v1);
		block[5]	= (short)(v2 + (v3 << 1));
		block[6]	= (short)(v0 - v1);
		block[7]	= (short)(v3 - (v2 << 1));
		block[8]	= (short)(r0 + r1);
		block[9]	= (short)(r2 + (r3 << 1));
		block[10]	= (short)(r0 - r1);
		block[11]	= (short)(r3 - (r2 << 1));
		block[12]	= (short)(u0 + u1);
		block[13]	= (short)(u2 + (u3 << 1));
		block[14]	= (short)(u0 - u1);
		block[15]	= (short)(u3 - (u2 << 1));

		/// 1-D forward vert direction.

		/// 1-D forward vert direction.

		/// 1st stage transform.
		s0 = (int)(block[0]	+ block[12]);
		s3 = (int)(block[0]	- block[12]);
		v0 = (int)(block[1]	+ block[13]);
		v3 = (int)(block[1]	- block[13]);
		r0 = (int)(block[2]	+ block[14]);
		r3 = (int)(block[2]	- block[14]);
		u0 = (int)(block[3]	+ block[15]);
		u3 = (int)(block[3]	- block[15]);
		s1 = (int)(block[4] + block[8]);
		s2 = (int)(block[4] - block[8]);
		v1 = (int)(block[5] + block[9]);
		v2 = (int)(block[5] - block[9]);
		r1 = (int)(block[6] + block[10]);
		r2 = (int)(block[6] - block[10]);
		u1 = (int)(block[7] + block[11]);
		u2 = (int)(block[7] - block[11]);

		/// 2nd stage transform.

		const int* pNorm = NormAdjust[_qm];

		/// [0]
		if( (s0 + s1) >= 0 )
			block[0] = (short)( (((s0 + s1) * pNorm[0]) + _f) >> _scale );
		else
			block[0] = (short)(-( (((-(s0 + s1)) * pNorm[0]) + _f) >> _scale ));
		/// [1]
		if( (v0 + v1) >= 0 )
			block[1] = (short)( (((v0 + v1) * pNorm[2]) + _f) >> _scale );
		else
			block[1] = (short)(-( (((-(v0 + v1)) * pNorm[2]) + _f) >> _scale ));
		/// [2]
		if( (r0 + r1) >= 0 )
			block[2] = (short)( (((r0 + r1) * pNorm[0]) + _f) >> _scale );
		else
			block[2] = (short)(-( (((-(r0 + r1)) * pNorm[0]) + _f) >> _scale ));
		/// [3]
		if( (u0 + u1) >= 0 )
			block[3] = (short)( (((u0 + u1) * pNorm[2]) + _f) >> _scale );
		else
			block[3] = (short)(-( (((-(u0 + u1)) * pNorm[2]) + _f) >> _scale ));

		/// [4]
		register int x4	= s2 + (s3 << 1);
		if( x4 >= 0 )
			block[4] = (short)( ((x4 * pNorm[2]) + _f) >> _scale );
		else
			block[4] = (short)(-( (((-x4) * pNorm[2]) + _f) >> _scale ));
		/// [5]
		register int x5	= v2 + (v3 << 1);
		if( x5 >= 0 )
			block[5] = (short)( ((x5 * pNorm[1]) + _f) >> _scale );
		else
			block[5] = (short)(-( (((-x5) * pNorm[1]) + _f) >> _scale ));
		/// [6]
		register int x6	= r2 + (r3 << 1);
		if( x6 >= 0 )
			block[6] = (short)( ((x6 * pNorm[2]) + _f) >> _scale );
		else
			block[6] = (short)(-( (((-x6) * pNorm[2]) + _f) >> _scale ));
		/// [7]
		register int x7	= u2 + (u3 << 1);
		if( x7 >= 0 )
			block[7] = (short)( ((x7 * pNorm[1]) + _f) >> _scale );
		else
			block[7] = (short)(-( (((-x7) * pNorm[1]) + _f) >> _scale ));

		/// [8]
		if( (s0 - s1) >= 0 )
			block[8] = (short)( (((s0 - s1) * pNorm[0]) + _f) >> _scale );
		else
			block[8] = (short)(-( (((-(s0 - s1)) * pNorm[0]) + _f) >> _scale ));
		/// [9]
		if( (v0 - v1) >= 0 )
			block[9] = (short)( (((v0 - v1) * pNorm[2]) + _f) >> _scale );
		else
			block[9] = (short)(-( (((-(v0 - v1)) * pNorm[2]) + _f) >> _scale ));
		/// [10]
		if( (r0 - r1) >= 0 )
			block[10] = (short)( (((r0 - r1) * pNorm[0]) + _f) >> _scale );
		else
			block[10] = (short)(-( (((-(r0 - r1)) * pNorm[0]) + _f) >> _scale ));
		/// [11]
		if( (u0 - u1) >= 0 )
			block[11] = (short)( (((u0 - u1) * pNorm[2]) + _f) >> _scale );
		else
			block[11] = (short)(-( (((-(u0 - u1)) * pNorm[2]) + _f) >> _scale ));

		/// [12]
		register int x12	= s3 - (s2 << 1);
		if( x12 >= 0 )
			block[12] = (short)( ((x12 * pNorm[2]) + _f) >> _scale );
		else
			block[12] = (short)(-( (((-x12) * pNorm[2]) + _f) >> _scale ));
		/// [13]
		register int x13	= v3 - (v2 << 1);
		if( x13 >= 0 )
			block[13] = (short)( ((x13 * pNorm[1]) + _f) >> _scale );
		else
			block[13] = (short)(-( (((-x13) * pNorm[1]) + _f) >> _scale ));
		/// [14]
		register int x14	= r3 - (r2 << 1);
		if( x14 >= 0 )
			block[14] = (short)( ((x14 * pNorm[2]) + _f) >> _scale );
		else
			block[14] = (short)(-( (((-x14) * pNorm[2]) + _f) >> _scale ));
		/// [15]
		register int x15	= u3 - (u2 << 1);
		if( x15 >= 0 )
			block[15] = (short)( ((x15 * pNorm[1]) + _f) >> _scale );
		else
			block[15] = (short)(-( (((-x15) * pNorm[1]) + _f) >> _scale ));

	}//end else...

}//end Transform.

#else ///< !FF4O16ITI2_UNROLL_LOOPS_AND_INTERLEAVE

void FastForward4x4On16x16ITImpl2::Transform(void* ptr)
{
  /// The mem format of the mb 16x16 with the 4x4 blks is linear contiguous.
  /// pmb = { blk[0][0][0...16], blk[0][1][0...16], ... , blk[3][3][0...16] }.
  short* pb1 = (short *)ptr;
  short* pb2 = &(pb1[64]);
  short* pb3 = &(pb1[128]);
  short* pb4 = &(pb1[192]);
  int i,j;

  if(_mode == IForwardTransform::TransformOnly)
  {
    /// 16 blks in the mb with linear mem addressing. (4 at a time)
    for (i = 0; i < 4; i++, pb1 += 16, pb2 += 16, pb3 += 16, pb4 += 16) 
    {
      /// 1-D forward horiz direction.
      for (j = 0; j < 16; j += 4) ///< 1 blk row at a time.
      {
        ///... 1st 2 blks
        /// 1st stage transform.
        int s0 = (int)(pb1[j] + pb1[j + 3]);
        int s3 = (int)(pb1[j] - pb1[j + 3]);
        int s1 = (int)(pb1[j + 1] + pb1[j + 2]);
        int s2 = (int)(pb1[j + 1] - pb1[j + 2]);
        int t0 = (int)(pb2[j] + pb2[j + 3]);
        int t3 = (int)(pb2[j] - pb2[j + 3]);
        int t1 = (int)(pb2[j + 1] + pb2[j + 2]);
        int t2 = (int)(pb2[j + 1] - pb2[j + 2]);
        /// 2nd stage transform.
        pb1[j]     = (short)(s0 + s1);
        pb1[j + 2] = (short)(s0 - s1);
        pb1[j + 1] = (short)(s2 + (s3 << 1));
        pb1[j + 3] = (short)(s3 - (s2 << 1));
        pb2[j]     = (short)(t0 + t1);
        pb2[j + 2] = (short)(t0 - t1);
        pb2[j + 1] = (short)(t2 + (t3 << 1));
        pb2[j + 3] = (short)(t3 - (t2 << 1));

        ///... 2nd 2 blks
        /// 1st stage transform.
        s0 = (int)(pb3[j] + pb3[j + 3]);
        s3 = (int)(pb3[j] - pb3[j + 3]);
        s1 = (int)(pb3[j + 1] + pb3[j + 2]);
        s2 = (int)(pb3[j + 1] - pb3[j + 2]);
        t0 = (int)(pb4[j] + pb4[j + 3]);
        t3 = (int)(pb4[j] - pb4[j + 3]);
        t1 = (int)(pb4[j + 1] + pb4[j + 2]);
        t2 = (int)(pb4[j + 1] - pb4[j + 2]);
        /// 2nd stage transform.
        pb3[j]     = (short)(s0 + s1);
        pb3[j + 2] = (short)(s0 - s1);
        pb3[j + 1] = (short)(s2 + (s3 << 1));
        pb3[j + 3] = (short)(s3 - (s2 << 1));
        pb4[j]     = (short)(t0 + t1);
        pb4[j + 2] = (short)(t0 - t1);
        pb4[j + 1] = (short)(t2 + (t3 << 1));
        pb4[j + 3] = (short)(t3 - (t2 << 1));
      }//end for j...

      /// 1-D forward vert direction.
      for (j = 0; j < 4; j++) /// 1 blk col at a time.
      {
        ///... 1st 2 blks
        /// 1st stage transform.
        int s0 = (int)(pb1[j] + pb1[j + 12]);
        int s3 = (int)(pb1[j] - pb1[j + 12]);
        int s1 = (int)(pb1[j + 4] + pb1[j + 8]);
        int s2 = (int)(pb1[j + 4] - pb1[j + 8]);
        int t0 = (int)(pb2[j] + pb2[j + 12]);
        int t3 = (int)(pb2[j] - pb2[j + 12]);
        int t1 = (int)(pb2[j + 4] + pb2[j + 8]);
        int t2 = (int)(pb2[j + 4] - pb2[j + 8]);
        /// 2nd stage transform.
        pb1[j]      = (short)(s0 + s1);
        pb1[j + 8]  = (short)(s0 - s1);
        pb1[j + 4]  = (short)(s2 + (s3 << 1));
        pb1[j + 12] = (short)(s3 - (s2 << 1));
        pb2[j]      = (short)(t0 + t1);
        pb2[j + 8]  = (short)(t0 - t1);
        pb2[j + 4]  = (short)(t2 + (t3 << 1));
        pb2[j + 12] = (short)(t3 - (t2 << 1));

        ///... 2nd 2 blks
        /// 1st stage transform.
        s0 = (int)(pb3[j] + pb3[j + 12]);
        s3 = (int)(pb3[j] - pb3[j + 12]);
        s1 = (int)(pb3[j + 4] + pb3[j + 8]);
        s2 = (int)(pb3[j + 4] - pb3[j + 8]);
        t0 = (int)(pb4[j] + pb4[j + 12]);
        t3 = (int)(pb4[j] - pb4[j + 12]);
        t1 = (int)(pb4[j + 4] + pb4[j + 8]);
        t2 = (int)(pb4[j + 4] - pb4[j + 8]);
        /// 2nd stage transform.
        pb3[j]      = (short)(s0 + s1);
        pb3[j + 8]  = (short)(s0 - s1);
        pb3[j + 4]  = (short)(s2 + (s3 << 1));
        pb3[j + 12] = (short)(s3 - (s2 << 1));
        pb4[j]      = (short)(t0 + t1);
        pb4[j + 8]  = (short)(t0 - t1);
        pb4[j + 4]  = (short)(t2 + (t3 << 1));
        pb4[j + 12] = (short)(t3 - (t2 << 1));
      }//end for j...
    }//end for i...

  }//end else if TransformOnly...
  else if (_mode == IForwardTransform::QuantOnly)
  {
    /// Scaling and quantisation are combined.
    const int* pNorm = NormAdjust[_qm];

    for (j = 0; j < 64; j++)  ///< 16 blks in the mb with linear mem addressing. (4 at a time)
    {
      const int norm = pNorm[ColSelector[j & 15]];

      if (*pb1 < 0) *pb1++ = (short)(-((((-(int)(*pb1)) * norm) + _f) >> _scale));
      else          *pb1++ = (short)((((int)(*pb1) * norm) + _f) >> _scale);

      if (*pb2 < 0) *pb2++ = (short)(-((((-(int)(*pb2)) * norm) + _f) >> _scale));
      else          *pb2++ = (short)((((int)(*pb2) * norm) + _f) >> _scale);

      if (*pb3 < 0) *pb3++ = (short)(-((((-(int)(*pb3)) * norm) + _f) >> _scale));
      else          *pb3++ = (short)((((int)(*pb3) * norm) + _f) >> _scale);

      if (*pb4 < 0) *pb4++ = (short)(-((((-(int)(*pb4)) * norm) + _f) >> _scale));
      else          *pb4++ = (short)((((int)(*pb4) * norm) + _f) >> _scale);
    }//end for j...
  }//end if QuantOnly...
  else ///< if _mode == TransformAndQuant)
  {
    short* pb = pb1;
    const int* pNorm = NormAdjust[_qm];

    for (i = 0; i < 16; i++, pb += 16) ///< 16 blks in the mb with linear mem addressing.
    {
      /// 1-D forward horiz direction.
      for (j = 0; j < 16; j += 4)
      {
        /// 1st stage transform.
        int s0 = (int)(pb[j] + pb[j + 3]);
        int s3 = (int)(pb[j] - pb[j + 3]);
        int s1 = (int)(pb[j + 1] + pb[j + 2]);
        int s2 = (int)(pb[j + 1] - pb[j + 2]);

        /// 2nd stage transform.
        pb[j]     = (short)(s0 + s1);
        pb[j + 2] = (short)(s0 - s1);
        pb[j + 1] = (short)(s2 + (s3 << 1));
        pb[j + 3] = (short)(s3 - (s2 << 1));
      }//end for j...

        /// 1-D forward vert direction.
      for (j = 0; j < 4; j++)
      {
        /// 1st stage transform.
        int s0 = (int)(pb[j] + pb[j + 12]);
        int s3 = (int)(pb[j] - pb[j + 12]);
        int s1 = (int)(pb[j + 4] + pb[j + 8]);
        int s2 = (int)(pb[j + 4] - pb[j + 8]);

        /// 2nd stage transform with scaling and quantisation.
        int w0 = s0 + s1;
        int w2 = s0 - s1;
        if (w0 < 0) pb[j] = (short)(-((((-w0) * pNorm[ColSelector[j]]) + _f) >> _scale));
        else        pb[j] = (short)(((w0 * pNorm[ColSelector[j]]) + _f) >> _scale);
        if (w2 < 0) pb[j + 8] = (short)(-((((-w2) * pNorm[ColSelector[j + 8]]) + _f) >> _scale));
        else        pb[j + 8] = (short)(((w2 * pNorm[ColSelector[j + 8]]) + _f) >> _scale);
        int w1 = s2 + (s3 << 1);
        int w3 = s3 - (s2 << 1);
        if (w1 < 0) pb[j + 4] = (short)(-((((-w1) * pNorm[ColSelector[j + 4]]) + _f) >> _scale));
        else        pb[j + 4] = (short)(((w1 * pNorm[ColSelector[j + 4]]) + _f) >> _scale);
        if (w3 < 0) pb[j + 12] = (short)(-((((-w3) * pNorm[ColSelector[j + 12]]) + _f) >> _scale));
        else        pb[j + 12] = (short)(((w3 * pNorm[ColSelector[j + 12]]) + _f) >> _scale);
      }//end for j...
    }//end for i...
  }//end else TransformAndQuant...

}//end Transform.

#endif 

/** Transfer forward IT.
The IT is performed on the input and the coeffs are written to 
the output.
@param pIn		: Input data.
@param pCoeff	: Output coeffs.
@return				:	none.
*/
void FastForward4x4On16x16ITImpl2::Transform(void* pIn, void* pCoeff)
{
	/// Copy to output and then do in-place inverse transform.
	memcpy(pCoeff, pIn, sizeof(short) * 256);
	Transform(pCoeff);
}//end Transform.

/** Set and get parameters for the implementation.
An Intra and Inter parameter and quantisation parameter are the only 
requirement for this implementation.
@param paramID	: Parameter to set/get.
@param paramVal	: Parameter value.
@return					: None (Set) or the param value (Get).
*/
void FastForward4x4On16x16ITImpl2::SetParameter(int paramID, int paramVal)
{
	switch(paramID)
	{
		case QUANT_ID:
			{
				// TODO: Put these calcs into a table.
				if(paramVal != _q)	///< Do we need to change member values or keep previous?
				{
					_q = paramVal;
          SetNewQP();
				}//end if q...
			}//end case QUANT_ID...
			break;
		case INTRA_FLAG_ID:
			_intra = paramVal;
      SetNewQP();
			break;
		default :
			/// Do nothing.
			break;
	}//end switch paramID...
}//end SetParameter.

int FastForward4x4On16x16ITImpl2::GetParameter(int paramID)
{
	int ret;
	switch(paramID)
	{
		case QUANT_ID:
			ret = _q;
			break;
		case INTRA_FLAG_ID:
			ret = _intra;
			break;
		default :
			ret = _intra;	///< Intra flag is returned as default.
			break;
	}//end switch paramID...

	return(ret);
}//end GetParameter.

/** Set internal quant members based on _q.
@return: none.
*/
void FastForward4x4On16x16ITImpl2::SetNewQP(void)
{
	_qm			= _q % 6;
	_qe			= _q/6;
	if(_intra)
		_f = (1 << (15+_qe))/3;
	else
		_f = (1 << (15+_qe))/6;
	_scale	= 15+_qe;
}//end SetNewQP.
