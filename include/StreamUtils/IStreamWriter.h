/** @file

MODULE						: IStreamWriter

TAG								: ISW

FILE NAME					: IStreamWriter.h

DESCRIPTION				: An abstract stream writer interface to be used 
										together with	IStreamBase interface.

REVISION HISTORY	:
									: 

COPYRIGHT					:

RESTRICTIONS			: 
===========================================================================
*/
#ifndef _ISTREAMWRITER_H
#define _ISTREAMWRITER_H

/*
---------------------------------------------------------------------------
	Class definition.
---------------------------------------------------------------------------
*/
class IStreamWriter
{
public:
	virtual ~IStreamWriter() {}

	/** Get current write position.
	Get the position in the linear array of bytes.
	@return : Position.
	*/
	virtual int GetWritePos(void) = 0;

	/** Bits/bytes available for writing.
	Determine the number of valid bits/bytes that are
	available to write.
	@return			: The num of bits/bytes.
	*/
	virtual int NumAvailToWrite(void) = 0;

	/** Write a single bit/byte.
	Write to the current position into the stream.
	@param val	: Bit/byte value to write.
	@return			: Bit/Byte was written.
	*/
	virtual int Write(int val) = 0;

	/** Write multiple bits/bytes to the stream.
	Write multiple bits/bytes into the current stream position.
	@param num	: No. of bits/bytes to write.
	@param src	: Values to write.
	@return			: Num bits/bytes written.
	*/
	virtual int Write(int num, void* src) = 0;

	/** Poke bits/bytes to the stream.
	Write multiple bits/bytes into the specified stream position 
	without disturbing the current stream position.
	@param loc	: Bit/Byte pos in stream.
	@param num	: No. of bits/bytes to write.
	@param src	: Values to write.
	@return			: Num bits/bytes written.
	*/
	virtual int Poke(int loc, int num, void* src) = 0;

};// end class IStreamWriter.

#endif	// _ISTREAMWRITER_H
