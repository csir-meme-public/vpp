/** @file

MODULE						: StreamChunkHeader

TAG								: SCH

FILE NAME					: StreamChunkHeader.h

DESCRIPTION				: A utility class to handle reading and writing chunk
										headers from and to byte streams. Chunk headers have
										a 4 byte id and a 4 byte size. The id labels are defined 
										here.

REVISION HISTORY	:
									: 

COPYRIGHT					: (c)VICS 2000-2006  all rights resevered - info@videocoding.com

RESTRICTIONS			: The information/data/code contained within this file is 
										the property of VICS limited and has been classified as 
										CONFIDENTIAL.
===========================================================================
*/
#ifndef _STREAMCHUNKHEADER_H
#define _STREAMCHUNKHEADER_H

#include "IStreamReader.h"
#include "IStreamWriter.h"

/*
---------------------------------------------------------------------------
	Object header definition.
---------------------------------------------------------------------------
*/
typedef struct _CHUNK_OBJ_HEADER
{
	// As defined by the constant identifiers.
	unsigned int id;
	// The num of bytes that follow this chunk to make
	// up the rest of the header.
	unsigned int size;

} CHUNK_OBJ_HEADER;

/*
---------------------------------------------------------------------------
	Class definition.
---------------------------------------------------------------------------
*/
class StreamChunkHeader
{
public:
	StreamChunkHeader();
	virtual ~StreamChunkHeader();

// Interface implementation.
public:
	virtual int Read(IStreamReader* bsr);
	virtual int Write(IStreamWriter* bsw);
	virtual void* GetObjHeader(void) { return((void *)(&_chunkHeader)); }
	virtual void  SetObjHeader(void* objHeader) { _chunkHeader = *((CHUNK_OBJ_HEADER *)objHeader); }

	// Memeber access.
	unsigned int GetId(void) { return(_chunkHeader.id); }
	unsigned int GetSize(void) { return(_chunkHeader.size); }
	void SetId(unsigned int id) { _chunkHeader.id = id; }
	void SetSize(unsigned int size) { _chunkHeader.size = size; }

// Private utilities.
protected:
	/** Read unterminated string from stream.
	Strings are stored as a 16 bit size followed by packed
	ASCII characters and need to be converted into a string.
	@param str	: Load into this string.
	@param bsr	: From this stream.
	@return			: Bytes read.
	*/
	static int ReadString(char** str, IStreamReader* bsr);

	/** Write unterminated string to stream.
	Strings are stored as a 16 bit size followed by packed
	ASCII characters and need to be converted from a string.
	@param str	: Write this string.
	@param bsw	: Into this stream.
	@return			: Bytes written.
	*/
	static int WriteString(char* str, IStreamWriter* bsw);

// Constant Identifiers.
public:
	static const unsigned int Vic;
	static const unsigned int Properties;
	static const unsigned int StreamProperties;
	static const unsigned int ContentDescription;
	static const unsigned int Data;

// Structured members.
protected:
	CHUNK_OBJ_HEADER	_chunkHeader;

};// end class StreamChunkHeader.

#endif	// _STREAMCHUNKHEADER_H
