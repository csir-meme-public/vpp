/** @file

MODULE						: IStreamBase

TAG								: ISB

FILE NAME					: IStreamBase.h

DESCRIPTION				: A IStreamBase Interface as an abstract base class to 
										accessing streams of bits/bytes. It is the common part
										to accessing IStreamReader and IStreamWriter interfaces. 
										It is typically used for buffer access of linear and 
										ring mem.

REVISION HISTORY	:
									: 

COPYRIGHT					: 

RESTRICTIONS			: 
===========================================================================
*/
#ifndef _ISTREAMBASE_H
#define _ISTREAMBASE_H

class IStreamBase
{
public:
	virtual ~IStreamBase() {}

	/** Set the stream to use.
	@param stream		: The stream ptr.
	@param size	: Size of the stream.
	@return			: none.
	*/
	virtual void SetStream(void* stream, int size) = 0;

	/** Reset to the beginning of the stream
	@return	: none.
	*/
	virtual void Reset(void) = 0; 

	/** Seek to a position in the stream.
	@param pos	: Pos to seek.
	@return			: Successful seek = 1, 0 = failed.
	*/
	virtual int Seek(int pos) = 0; 

	/** Get the current position in the stream.
	@return	: Current position.
	*/
	virtual int GetStreamPos(void) = 0;

	/** Get the size of the stream.
	@return	: Size.
	*/
	virtual int GetStreamSize(void) = 0;

};// end class IStreamBase.

#endif	//_ISTREAMBASE_H
