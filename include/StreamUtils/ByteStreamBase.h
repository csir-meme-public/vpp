/** @file

MODULE						: ByteStreamBase

TAG								: BSB

FILE NAME					: ByteStreamBase.h

DESCRIPTION				: A base class to contain a contiguous mem array to be
										byte accessible. The mem is not owned by the class. 
										Derived classes handle the reading and writing, 
										ByteStreamReader and ByteStreamWriter.

REVISION HISTORY	:
									: 

COPYRIGHT					:

RESTRICTIONS			: 
===========================================================================
*/
#ifndef _BYTESTREAMBASE_H
#define _BYTESTREAMBASE_H


#include "IStreamBase.h"

/*
---------------------------------------------------------------------------
	Class definition.
---------------------------------------------------------------------------
*/
class ByteStreamBase : public IStreamBase
{
public:
	ByteStreamBase()	
		{_byteStream = NULL; _byteSize = 0; _bytePos = 0; }
	virtual ~ByteStreamBase()	{ }

	virtual void SetStream(void* stream, int size)	
		{ _byteStream = (unsigned char *)stream; 
			_byteSize = size; 
			_bytePos = 0; 
		}//end SetStream.

public:
	// Interface implementation.
	virtual void Reset(void) 
		{ _bytePos = 0; }

	virtual int Seek(int pos) 
		{ _bytePos	= pos; 
			return(1); 
		}//end Seek.

	virtual int GetStreamPos(void) { return(_bytePos); }
	virtual int GetStreamSize(void) { return(_byteSize); }
	virtual void SetStreamSize(int byteSize) { _byteSize = byteSize; }

protected:
	unsigned char*	_byteStream;		// Reference to byte array.
	int							_byteSize;			// Valid bytes in stream.
	// Current location.
	int							_bytePos;

};// end class ByteStreamBase.

#endif	// _BYTESTREAMBASE_H
