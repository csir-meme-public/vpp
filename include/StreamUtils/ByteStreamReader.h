/** @file

MODULE						: ByteStreamReader

TAG								: BSR

FILE NAME					: ByteStreamReader.h

DESCRIPTION				: A byte stream reader implementation of the ByteStreamBase
										base class. Add the functionality to do the byte reading.
										Basic operation:
											ByteStreamReader* pBsr = new ByteStreamReader();
											pBsr->SetStream((void *)pStream, (streamLen * sizeof(pStream[0])));
											char buff[20];
											pBsr->Read(5, buff);
											.
											.
											delete pBsr;

REVISION HISTORY	:
									: 

COPYRIGHT					: (c)VICS 2000-2006  all rights resevered - info@videocoding.com

RESTRICTIONS			: The information/data/code contained within this file is 
										the property of VICS limited and has been classified as 
										CONFIDENTIAL.
===========================================================================
*/
#ifndef _BYTESTREAMREADER_H
#define _BYTESTREAMREADER_H


#include "ByteStreamBase.h"
#include "IStreamReader.h"

/*
---------------------------------------------------------------------------
	Class definition.
---------------------------------------------------------------------------
*/
class ByteStreamReader : public ByteStreamBase, public IStreamReader
{
public:
	ByteStreamReader();
	virtual ~ByteStreamReader();

// Interface implementation.
public:
	/** Get current read position.
	Get the position in the linear array of bytes.
	@return : Position.
	*/
	int GetReadPos(void) { return(_bytePos); }

	/** Bytes available for reading.
	Determine the number of valid bytes that are
	available to read.
	@return			: The num of bytes.
	*/
	int NumAvailToRead(void);

	/** Read a single byte.
	Read from the current byte position in the stream.
	@return			: The byte.
	*/
	int Read(void);

	/** Read bytes from the stream.
	Read multiple bytes from the current stream position.
	@param num	: No. of bytes to read.
	@param dst	: Buffer to read into.
	@return			: Num of bytes read.
	*/
	int Read(int num, void* dst);

	/** Peek bytes in the stream.
	Read multiple bytes from the specified stream position 
	without disturbing the	current stream position.
	@param loc	: Byte pos in stream.
	@param num	: No. of bytes to read.
	@param dst	: Buffer to read into.
	@return			: Num of bytes read.
	*/
	int Peek(int loc, int num, void* dst);

};// end class ByteStreamReader.

#endif	// _BITSTREAMREADER_H
