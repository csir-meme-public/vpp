/** @file

MODULE						: ByteStreamWriter

TAG								: BSW

FILE NAME					: ByteStreamWriter.h

DESCRIPTION				: A byte stream writer implementation of the ByteStreamBase
										base class. Add the functionality to do the byte writing.
										Basic operation:
											ByteStreamWriter* pBsr= new ByteStreamWriter();
											pBsr->SetStream((void *)pStream, (streamLen * sizeof(pStream[0])));
											char* buff = "ksfonfoeee...";
											pBsr->Write(strlen(buff), buff);
											.
											.
											delete pBsr;

REVISION HISTORY	:
									: 

COPYRIGHT					: (c)VICS 2000-2006  all rights resevered - info@videocoding.com

RESTRICTIONS			: The information/data/code contained within this file is 
										the property of VICS limited and has been classified as 
										CONFIDENTIAL.
===========================================================================
*/
#ifndef _BYTESTREAMWRITER_H
#define _BYTESTREAMWRITER_H

#include "ByteStreamBase.h"
#include "IStreamWriter.h"

/*
---------------------------------------------------------------------------
	Class definition.
---------------------------------------------------------------------------
*/
class ByteStreamWriter : public ByteStreamBase, public IStreamWriter
{
public:
	ByteStreamWriter();
	virtual ~ByteStreamWriter();

// Interface implementation.
public:
	/** Get current write position.
	Get the position in the linear array of bytes.
	@return : Position.
	*/
	int GetWritePos(void) { return(_bytePos); }

	/** Bytes available for writing.
	Determine the number of valid bytes that are
	available to write.
	@return			: The num of bytes.
	*/
	int NumAvailToWrite(void);

	/** Write a single byte.
	Write to the current byte position into the stream.
	@param val	: Byte value to write.
	@return			: Byte was written.
	*/
	int Write(int val);

	/** Write bytes to the stream.
	Write multiple byte into the current stream position.
	@param num	: No. of bytes to write.
	@param src	: Byte values to write.
	@return			: Num bytes written.
	*/
	int Write(int num, void* src);

	/** Poke bytes to the stream.
	Write multiple bytes into the specified stream position 
	without disturbing the current stream position.
	@param loc	: Byte pos in stream.
	@param num	: No. of bytes to write.
	@param src	: Byte values to write.
	@return			: Num bytes written.
	*/
	int Poke(int loc, int num, void* src);

};// end class ByteStreamWriter.

#endif	// _BYTESTREAMWRITER_H
