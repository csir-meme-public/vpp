/** @file

MODULE						: IStreamReader

TAG								: ISR

FILE NAME					: IStreamReader.h

DESCRIPTION				: An abstract stream reader interface to be used 
										together with	IStreamBase interface.

REVISION HISTORY	:
									: 

COPYRIGHT					: 

RESTRICTIONS			: 
===========================================================================
*/
#ifndef _ISTREAMREADER_H
#define _ISTREAMREADER_H

/*
---------------------------------------------------------------------------
	Class definition.
---------------------------------------------------------------------------
*/
class IStreamReader
{
public:
	virtual ~IStreamReader() {}

	/** Get current read position.
	Get the position in the linear array of bytes.
	@return : Position.
	*/
	virtual int GetReadPos(void) = 0;

	/** Bits/bytes available for reading.
	Determine the number of valid bits/bytes that are
	available to read.
	@return			: The num of bits/bytes.
	*/
	virtual int NumAvailToRead(void) = 0;

	/** Read a single bit/byte.
	Read from the current position in the stream.
	@return			: The bit/byte.
	*/
	virtual int Read(void) = 0;

	/** Read from the stream.
	Read multiple bits/bytes from the current stream position.
	@param num	: No. of bits/bytes to read.
	@param dst	: Buffer to read into.
	@return			: Num of bits/bytes read.
	*/
	virtual int Read(int num, void* dst) = 0;

	/** Peek bits/bytes in the stream.
	Read multiple bits/bytes from the specified stream position 
	without disturbing the current stream position.
	@param loc	: Pos in stream.
	@param num	: No. of bits/bytes to read.
	@param dst	: Buffer to read into.
	@return			: Num of bits/bytes read.
	*/
	virtual int Peek(int loc, int num, void* dst) = 0;

};// end class IStreamReader.

#endif	// _ISTREAMREADER_H
